#!/bin/bash
#
# A simple wrapper to run kpet via its source code
#

WSROOT=${WSROOT:-"/tmp/foo"}

if [[ ! -d $WSROOT/cki-lib ]]; then
	git clone https://gitlab.com/cki-project/cki-lib.git $WSROOT/cki-lib
fi

if [[ ! -d $WSROOT/kpet ]]; then
	git clone https://gitlab.com/cki-project/kpet.git    $WSROOT/kpet
fi

if [[ ! -d $WSROOT/kpet-db ]]; then
	git clone git@gitlab.com:idorax-cki-project/kpet-db.git $WSROOT/kpet-db
fi

export CKI_LIB_PATH=$WSROOT/cki-lib
export KPET_PATH=$WSROOT/kpet
export PYTHONPATH=$CKI_LIB_PATH:$KPET_PATH:$PYTHONPATH
export KPET_DB=$WSROOT/kpet-db

echo
echo ">>> export CKI_LIB_PATH=$CKI_LIB_PATH"
echo ">>> export KPET_PATH=$KPET_PATH"
echo ">>> export PYTHONPATH=$PYTHONPATH"
echo ">>> export KPET_DB=$KPET_DB"
echo ">>> alias  kpetx='python3 -m kpet --db $KPET_DB'"
echo
sleep 5

python3 -m kpet --db $KPET_DB "$@"
exit $?
