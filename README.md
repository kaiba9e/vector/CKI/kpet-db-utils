# kpet-db-utils

## How to run kpet via source code

e.g.
```bash
export WSROOT=/var/tmp/foo && mkdir /var/tmp/foo 

git clone https://gitlab.com/cki-project/cki-lib.git $WSROOT/cki-lib
git clone https://gitlab.com/cki-project/kpet.git    $WSROOT/kpet
git clone git@gitlab.com:idorax-cki-project/kpet-db.git $WSROOT/kpet-db

export CKI_LIB_PATH=$WSROOT/cki-lib
export KPET_PATH=$WSROOT/kpet
export PYTHONPATH=$CKI_LIB_PATH:$KPET_PATH:$PYTHONPATH
export KPET_DB=$WSROOT/kpet-db
python3 -m kpet --db $KPET_DB ...
```
